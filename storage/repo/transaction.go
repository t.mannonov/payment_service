package repo

import "time"

var (
	TransactionStatusPaid    = "paid"
	TransactionStatusPending = "pending"
	TransactionStatusFailed  = "failed"
)

type Transaction struct {
	ID                      int64
	StripeCheckoutSessionID string
	Amount                  float64
	OrderID                 int64
	Status                  string
	CreatedAt               time.Time
	UpdatedAt               time.Time
}

type UpdateTransactionStatus struct {
	StripeCheckoutSessionID string
	Status                  string
}

type TransactionStorageI interface {
	Create(tr *Transaction) (*Transaction, error)
	Get(id int64) (*Transaction, error)
	UpdateStatus(req *UpdateTransactionStatus) error
}
