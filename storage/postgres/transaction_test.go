package postgres_test

import (
	"testing"

	"github.com/bxcodec/faker/v4"
	"github.com/stretchr/testify/require"
	"gitlab.com/t.mannonov/payment_service/storage/repo"
)

func createTransaction(t *testing.T) *repo.Transaction {
	tr, err := strg.Transaction().Create(&repo.Transaction{
		StripeCheckoutSessionID: faker.UUIDHyphenated(),
		Amount:                  1000.45,
		OrderID:                 4,
	})
	require.NoError(t, err)
	require.NotEmpty(t, tr)

	return tr
}

func TestCreateTransaction(t *testing.T) {
	createTransaction(t)
}

func TestGetTransaction(t *testing.T) {
	tr := createTransaction(t)

	resp, err := strg.Transaction().Get(tr.ID)
	require.NoError(t, err)
	require.NotEmpty(t, resp)
}

func TestUpdateTransactionStatus(t *testing.T) {
	tr := createTransaction(t)

	err := strg.Transaction().UpdateStatus(&repo.UpdateTransactionStatus{
		StripeCheckoutSessionID: tr.StripeCheckoutSessionID,
		Status:                  repo.TransactionStatusPaid,
	})
	require.NoError(t, err)
}
