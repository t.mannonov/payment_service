package postgres

import (
	"database/sql"

	"github.com/jmoiron/sqlx"
	"gitlab.com/t.mannonov/payment_service/storage/repo"
)

type transactionRepo struct {
	db *sqlx.DB
}

func NewTransaction(db *sqlx.DB) repo.TransactionStorageI {
	return &transactionRepo{
		db: db,
	}
}

func (ur *transactionRepo) Create(tr *repo.Transaction) (*repo.Transaction, error) {
	query := `
		INSERT INTO transactions(
			stripe_checkout_session_id,
			amount,
			order_id
		) VALUES($1, $2, $3)
		RETURNING 
			id,
			status, 
			created_at,
			updated_at
	`

	row := ur.db.QueryRow(
		query,
		tr.StripeCheckoutSessionID,
		tr.Amount,
		tr.OrderID,
	)

	err := row.Scan(
		&tr.ID,
		&tr.Status,
		&tr.CreatedAt,
		&tr.UpdatedAt,
	)
	if err != nil {
		return nil, err
	}

	return tr, nil
}

func (ur *transactionRepo) Get(id int64) (*repo.Transaction, error) {
	var result repo.Transaction

	query := `
		SELECT
			id,
			stripe_checkout_session_id,
			amount,
			order_id,
			status,
			created_at,
			updated_at
		FROM transactions
		WHERE id=$1
	`

	row := ur.db.QueryRow(query, id)
	err := row.Scan(
		&result.ID,
		&result.StripeCheckoutSessionID,
		&result.Amount,
		&result.OrderID,
		&result.Status,
		&result.CreatedAt,
		&result.UpdatedAt,
	)
	if err != nil {
		return nil, err
	}

	return &result, nil
}

func (ur *transactionRepo) UpdateStatus(req *repo.UpdateTransactionStatus) error {
	query := `UPDATE transactions SET status=$1 WHERE stripe_checkout_session_id=$2`

	result, err := ur.db.Exec(query, req.Status, req.StripeCheckoutSessionID)
	if err != nil {
		return err
	}

	if count, _ := result.RowsAffected(); count == 0 {
		return sql.ErrNoRows
	}

	return nil
}
