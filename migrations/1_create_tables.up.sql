CREATE TABLE IF NOT EXISTS "transactions"(
    "id" SERIAL PRIMARY KEY,
    "stripe_checkout_session_id" VARCHAR NOT NULL UNIQUE,
    "amount" DECIMAL(18,2) NOT NULL,
    "status" VARCHAR NOT NULL DEFAULT 'pending' CHECK ("status" in ('pending', 'paid', 'failed')),
    "order_id" INTEGER NOT NULL,
    "created_at" TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP
);
