package v1

import (
	"encoding/json"
	"io"
	"log"
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/stripe/stripe-go/v74"
	"github.com/stripe/stripe-go/v74/webhook"
	"gitlab.com/t.mannonov/payment_service/storage/repo"
)

// Webhook godoc
// @Summary Stripe webhook
// @Tags payment
// @Description stripe webhook
// @Accept json
// @Produce json
// @Param data body interface{} true "webhook data"
// @Success 200 {object} models.ResponseOK
// @Router /payment/stripe-webhook [post]
func (h *handlerV1) StripeWebhook(c *gin.Context) {
	stripe.Key = h.cfg.Stripe.SecretKey

	payload, err := io.ReadAll(c.Request.Body)
	if err != nil {
		c.JSON(http.StatusBadRequest, errorResponse(err))
		return
	}

	endpointSecret := h.cfg.Stripe.EndpointSecret
	event, err := webhook.ConstructEvent(
		payload,
		c.GetHeader("Stripe-Signature"),
		endpointSecret,
	)
	if err != nil {
		log.Printf("Authorize webhook: error %v\n", err)
		c.JSON(http.StatusUnauthorized, errorResponse(err))
		return
	}

	err = h.handleStripeWebhook(&event)
	if err != nil {
		log.Printf("Hanle webhook: error %v\n", err)
		c.JSON(http.StatusInternalServerError, errorResponse(err))
		return
	}

	c.Status(http.StatusOK)
}

func (h *handlerV1) handleStripeWebhook(event *stripe.Event) error {
	switch event.Type {
	case "checkout.session.completed":
		err := h.HandleCheckoutSessionCompletedEvent(event.Data.Raw)
		if err != nil {
			return err
		}
	case "checkout.session.failed":
		err := h.HandleCheckoutSessionFailedEvent(event.Data.Raw)
		if err != nil {
			return err
		}
	default:
		log.Printf("Stripe webhook unhandled event type: %s\n", event.Type)
	}

	return nil
}

func (h *handlerV1) HandleCheckoutSessionCompletedEvent(payload []byte) error {
	var cs stripe.CheckoutSession

	err := json.Unmarshal(payload, &cs)
	if err != nil {
		return err
	}

	stripe.Key = h.cfg.Stripe.SecretKey

	if string(cs.Mode) != string(stripe.CheckoutSessionModePayment) {
		return nil
	}

	if cs.PaymentStatus == "paid" {
		err = h.storage.Transaction().UpdateStatus(&repo.UpdateTransactionStatus{
			StripeCheckoutSessionID: cs.ID,
			Status:                  repo.TransactionStatusPaid,
		})
		if err != nil {
			return err
		}
	}

	return nil
}

func (h *handlerV1) HandleCheckoutSessionFailedEvent(payload []byte) error {
	var cs stripe.CheckoutSession

	err := json.Unmarshal(payload, &cs)
	if err != nil {
		return err
	}

	stripe.Key = h.cfg.Stripe.SecretKey

	if string(cs.Mode) != string(stripe.CheckoutSessionModePayment) {
		return nil
	}

	err = h.storage.Transaction().UpdateStatus(&repo.UpdateTransactionStatus{
		StripeCheckoutSessionID: cs.ID,
		Status:                  repo.TransactionStatusFailed,
	})
	if err != nil {
		return err
	}

	return nil
}
