package v1

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/stripe/stripe-go/v74"
	"github.com/stripe/stripe-go/v74/checkout/session"
	"gitlab.com/t.mannonov/payment_service/api/models"
	"gitlab.com/t.mannonov/payment_service/storage/repo"
)

// @Router /payment/link [post]
// @Summary Create a payment link
// @Description Create a payment link
// @Tags payment
// @Accept json
// @Produce json
// @Param data body models.CreatePaymentLink true "Data"
// @Success 200 {object} models.CreatePaymentLinkResponse
// @Failure 400 {object} models.ErrorResponse
// @Failure 500 {object} models.ErrorResponse
func (h *handlerV1) CreatePaymentLink(c *gin.Context) {
	var (
		req models.CreatePaymentLink
	)

	err := c.ShouldBindJSON(&req)
	if err != nil {
		c.JSON(http.StatusBadRequest, errorResponse(err))
		return
	}

	url, err := h.createCheckoutSession(&req)
	if err != nil {
		c.JSON(http.StatusInternalServerError, errorResponse(err))
		return
	}

	c.JSON(http.StatusOK, models.CreatePaymentLinkResponse{
		Url: url,
	})
}

func (h *handlerV1) createCheckoutSession(req *models.CreatePaymentLink) (string, error) {
	stripe.Key = h.cfg.Stripe.SecretKey

	order := getOrder(req.OrderID)

	params := &stripe.CheckoutSessionParams{
		LineItems:          []*stripe.CheckoutSessionLineItemParams{},
		PaymentMethodTypes: stripe.StringSlice([]string{"card"}),
		Mode:               stripe.String(string(stripe.CheckoutSessionModePayment)),
		SuccessURL:         stripe.String(req.SuccessUrl),
		CancelURL:          stripe.String(req.CancelUrl),
	}

	for _, item := range order.OrderItems {
		unitAmount := item.Price * 100
		params.LineItems = append(params.LineItems, &stripe.CheckoutSessionLineItemParams{
			PriceData: &stripe.CheckoutSessionLineItemPriceDataParams{
				Currency: stripe.String(string(stripe.CurrencyUSD)),
				ProductData: &stripe.CheckoutSessionLineItemPriceDataProductDataParams{
					Name: stripe.String(item.ProductName),
				},
				UnitAmount: stripe.Int64(int64(unitAmount)),
			},
			Quantity: stripe.Int64(int64(item.Quantity)),
		})
	}

	deliveryUnitAmount := order.DeliveryPrice * 100
	params.LineItems = append(params.LineItems, &stripe.CheckoutSessionLineItemParams{
		PriceData: &stripe.CheckoutSessionLineItemPriceDataParams{
			Currency: stripe.String(string(stripe.CurrencyUSD)),
			ProductData: &stripe.CheckoutSessionLineItemPriceDataProductDataParams{
				Name: stripe.String("Delivery fee"),
			},
			UnitAmount: stripe.Int64(int64(deliveryUnitAmount)),
		},
		Quantity: stripe.Int64(int64(1)),
	})

	s, err := session.New(params)
	if err != nil {
		return "", err
	}

	_, err = h.storage.Transaction().Create(&repo.Transaction{
		OrderID:                 req.OrderID,
		StripeCheckoutSessionID: s.ID,
		Amount:                  order.Total,
	})
	if err != nil {
		return "", err
	}

	return s.URL, nil
}

func getOrder(id int64) *models.Order {
	return &models.Order{
		ID:            1,
		Total:         100,
		Status:        "new",
		DeliveryPrice: 10,
		OrderItems: []*models.OrderItem{
			{
				ProductName: "Mouse",
				ProductID:   1,
				Price:       45,
				Quantity:    2,
			},
		},
	}
}

// create checkout session
