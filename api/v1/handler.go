package v1

import (
	"gitlab.com/t.mannonov/payment_service/api/models"
	"gitlab.com/t.mannonov/payment_service/config"
	"gitlab.com/t.mannonov/payment_service/storage"
)

type handlerV1 struct {
	cfg     *config.Config
	storage storage.StorageI
}

func New(cfg *config.Config, strg storage.StorageI) *handlerV1 {
	return &handlerV1{cfg: cfg, storage: strg}
}

func errorResponse(err error) *models.ErrorResponse {
	return &models.ErrorResponse{
		Error: err.Error(),
	}
}
