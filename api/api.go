package api

import (
	"github.com/gin-gonic/gin"
	v1 "gitlab.com/t.mannonov/payment_service/api/v1"
	"gitlab.com/t.mannonov/payment_service/config"
	"gitlab.com/t.mannonov/payment_service/storage"

	swaggerFiles "github.com/swaggo/files"     // swagger embed files
	ginSwagger "github.com/swaggo/gin-swagger" // gin-swagger middleware

	_ "gitlab.com/t.mannonov/payment_service/api/docs" // for swagger
)

// @title           Swagger for payment api
// @version         1.0
// @description     This is a payment service api.
// @BasePath  /v1
// @securityDefinitions.apikey ApiKeyAuth
// @in header
// @name Authorization
// @Security ApiKeyAuth
func New(cfg *config.Config, strg storage.StorageI) *gin.Engine {
	router := gin.Default()

	handlerV1 := v1.New(cfg, strg)

	// API
	apiV1 := router.Group("/v1")
	apiV1.POST("/payment/link", handlerV1.CreatePaymentLink)
	apiV1.POST("/payment/stripe-webhook", handlerV1.StripeWebhook)

	router.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))

	return router
}
