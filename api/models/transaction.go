package models

import "time"

type CreatePaymentLink struct {
	OrderID    int64  `json:"order_id" binding:"required"`
	CancelUrl  string `json:"cancel_url" binding:"required" example:"http://cancel.com/"`
	SuccessUrl string `json:"success_url" binding:"required" example:"http://success.com/"`
}

type CreatePaymentLinkResponse struct {
	Url string `json:"url"`
}

type OrderItem struct {
	ProductName string
	ProductID   int64
	Price       float64
	Quantity    int32
}

type Order struct {
	ID            int64
	Total         float64
	OrderItems    []*OrderItem
	Address       string
	PaymentStatus string
	Status        string
	DeliveryPrice float64
	CreatedAt     time.Time
}
